# Readme for mta-hosting-optimizer in Bash

Tried to create the task in Bash, but realized that Python is better. So heres my half-backed aproach.\
Found below mentioned Sampo which is basically a scripts to simulate an api within Bash.\
There are two variants:
* A docker container which cant pick up the environment variable
* A script whoch can pick up the environment variable

To get Sampo to work all you need to do is adding a simple line. See sampo/sampo.conf line 69.

## Prerequisites
* [Docker](https://www.docker.com/)
* [Sampo](https://github.com/jacobsalmela/sampo?ref=jacobsalmela.com)

## Steps for creating container
1. Setting the environment variable THRESHOLD is not working in this variant 
2. Before building the container set proper ip address of your host system (localhost is not working): Line 3 in file sampo/scripts/mta.sh 
3. Create Docker image: `docker compose --profile mta-hosting-optimizer-build build`
4. Start container with finalized image: `docker compose --profile mta-hosting-optimizer up -d`
5. Of course the [ip-config](https://gitlab.com/Tulpenfieber/ip-config/activity) container should run on your host on port 8080
6. To get output from container: `curl http://localhost/8082/mta`

## Steps for just running the script
1. Set the environment variable THRESHOLD or not: `export THRESHOLD="10"`
2. Set localhost as target : Line 3 in file sampo/scripts/mta.sh
3. Got to scripts folder: `cd bash/sampo/scripts`
4. Run script: `./mta.sh`
5. 