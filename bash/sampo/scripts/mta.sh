#!/usr/bin/env bash

TARGET_URL="192.168.0.36"

if [[ ${THRESHOLD} == "" ]]; then
    THRESHOLD="1"
else
    THRESHOLD="${THRESHOLD}"
fi

declare HOSTNAMES STATUS_TRUE

getHostNames() {
    HOSTNAMES=$(curl -s http://${TARGET_URL}:8080/ip-configs | jq '.[] | .hostname' | sed 's/"//g' | sort -u)
}

countStatus() {
    for hostname in $HOSTNAMES; do
        STATUS_TRUE=$(curl -s http://${TARGET_URL}:8080/ip-configs | jq --arg HOSTNAME "$hostname" '.[] | select(.hostname==$HOSTNAME) | select(.active==true) | .hostname' | wc -l)
        if [[ "${STATUS_TRUE}" -le "${THRESHOLD}" ]]; then
            echo "{"
            echo "$hostname"
            echo "}"
        fi
    done
}

getHostNames
countStatus
