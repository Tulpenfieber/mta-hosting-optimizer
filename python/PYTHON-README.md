# Readme for mta-hosting-optimizer in Python

Miniconda is a good Python manager where you can create as many environments you need. The main benefit is that\
you don't need to install any system packages via pip or apt. With that you are always system independent and portable.\
Just define required packages in `requirements.txt` file, conda does the rest.

Project is based on following tutorials:
* [How to create an API in Python](https://anderfernandez.com/en/blog/how-to-create-api-python/)
* [HTTP Request in Python](https://www.datacamp.com/tutorial/making-http-requests-in-python)

There is a ./src/dev.py file which was my development file. Later the transition to main.py happened.

## Prerequisites
* [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
* [FastApi](https://fastapi.tiangolo.com/)
* [Uvicorn](https://www.uvicorn.org/)

## Setup miniconda and Python environment
* Go to you project folder
* Download miniconda to local folder and install it to home folder. Just say yes to everyhting : `wget https://repo.anaconda.com/miniconda/Miniconda3-py311_23.5.2-0-Linux-x86_64.sh && chmod +x Miniconda3-py311_23.5.2-0-Linux-x86_64.sh && ./Miniconda3-py311_23.5.2-0-Linux-x86_64.sh`
* HINT: if you want to get rid of autoinitilization in shell, delete the last lines in your ~/.bashrc
* Create your python environment: `conda create --prefix $(pwd)/python-venv --file requirements.txt  python=3.7`
* If you have problems with conda finding packages run this: `conda config --apend channels conda-forge`
* Activate conda environment: `conda activate python-venv/`

## Start uvicorn webserver
* Go to project folder ./src
* Of course the [ip-config](https://gitlab.com/Tulpenfieber/ip-config/activity) container should run on your host on port 8080
* If you want to set the THRESHOLD environment variable: `export THRESHOLD="10"`
* Start uvicorn server on port 8000: `uvicorn main:app`
* Or to autoreload changes on main.py: `uvicorn main:app --reload`
* To see FastAPI documentation go to URL: `http://localhost:8000/docs`
* To see mta-hosting-optimizer: `http://localhost:8000/hostnames`
