import requests
import os
from fastapi import FastAPI
app = FastAPI()


@app.get("/hostnames")
def mta_hosting_optimizer():
    # import 'export THRESHOLD="X"' from environment variable
    threshold = os.getenv('THRESHOLD')
    if not os.getenv('THRESHOLD'):
        threshold = 1

    # get response from ip-config service
    ip_config_url = "http://localhost:8080/ip-configs"
    ip_config_response = requests.get(ip_config_url)
    ip_config_response_json = ip_config_response.json()

    # filter out hostnames to have a list
    hostlist = []
    for item in ip_config_response_json:
        if item['hostname'] not in hostlist:
            hostlist.append(item['hostname'])

    # count hostnames which have status active true
    result = []
    for hostname in hostlist:
        count = 0
        for item in ip_config_response_json:
            if item['active'] and item['hostname'] == hostname:
                count = count + 1
        if count <= int(threshold):
            result.append(hostname)
    return result
