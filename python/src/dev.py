import requests
import json


def mta_hosting_optimizer(threshold=1):
    # get response from ip-config service
    ip_config_url = "http://localhost:8080/ip-configs"
    ip_config_response = requests.get(ip_config_url)
    ip_config_response_json = ip_config_response.json()

    # filter out hostnames to have a list
    hostlist = []
    for item in ip_config_response_json:
        if item['hostname'] not in hostlist:
            hostlist.append(item['hostname'])

    result = []
    for hostname in hostlist:
        count = 0
        templist = []
        for item in ip_config_response_json:
            if item['active'] and item['hostname'] == hostname:
                count = count + 1
        if count <= threshold:
            result.append(hostname)
    result = json.dumps(result)
    print(result)

def main():
    mta_hosting_optimizer()

if __name__ == "__main__":
    main()